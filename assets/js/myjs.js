$(document).ready(function(){
	$('.close-notif').on('click',function(){
		$(this).parents('div').remove();
	});
	
	$('#table_admin').DataTable();
	$('.tb_datatables').DataTable();

  	$(document).on('click', '.datepicker', function(){
	   $(this).datepicker({
	      changeMonth: true,
	      changeYear: true,
	      dateFormat: "yy-mm-dd",
	      yearRange: "1920:2050"
	   }).focus();
	   $(this).removeClass('datepicker'); 
	});
  	
  	$('.add-dataktp').on('click',function(){
  		var x = $('.hidden-form-kk').children('.form-kk-each').clone();
  		$('.addmore-container').append(x);
  	});

  	$(document).on('click','.del-form-kk',function(){
		var ini_click = $(this);
		ini_click.parent('.form-kk-each').remove();
	});
});