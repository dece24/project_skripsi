-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.35-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for proj_skripsi
DROP DATABASE IF EXISTS `proj_skripsi`;
CREATE DATABASE IF NOT EXISTS `proj_skripsi` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci */;
USE `proj_skripsi`;

-- Dumping structure for table proj_skripsi.tb_admin
DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE IF NOT EXISTS `tb_admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `username_admin` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password_admin` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `level_admin` tinyint(4) NOT NULL DEFAULT '0',
  `admin_active` tinyint(2) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table proj_skripsi.tb_admin: ~0 rows (approximately)
DELETE FROM `tb_admin`;
/*!40000 ALTER TABLE `tb_admin` DISABLE KEYS */;
INSERT INTO `tb_admin` (`id_admin`, `username_admin`, `password_admin`, `level_admin`, `admin_active`, `date_created`) VALUES
	(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1, 1, '2018-11-27 00:59:15');
/*!40000 ALTER TABLE `tb_admin` ENABLE KEYS */;

-- Dumping structure for table proj_skripsi.tb_user
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE IF NOT EXISTS `tb_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `username_user` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `email_user` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table proj_skripsi.tb_user: ~0 rows (approximately)
DELETE FROM `tb_user`;
/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
INSERT INTO `tb_user` (`id_user`, `nama`, `username_user`, `email_user`, `password`, `is_active`, `date_created`) VALUES
	(1, 'dian ', 'diandece', 'diandwi1@yahoo.com', '2f0afd2259aa0f6b57b8313327e39757594b5483', 0, '2018-11-29 02:12:35'),
	(2, 'dece', 'dece', 'dece@yahoo.com', '2f0afd2259aa0f6b57b8313327e39757594b5483', 0, '2018-11-29 03:14:18'),
	(3, 'Si Dece', 'deceaja', 'gusti@gusti.com', '3b33e72b098bb8781472430a9bd9c472f711154f', 0, '2018-11-29 03:23:55');
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
