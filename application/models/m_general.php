<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_general extends CI_Model {

	public function __construct() {
		parent::__construct();
		// load library/helper
	}
	function insert_transact($tablename,$data=array()) {
		// $this->db->db_debug = FALSE;

		// $this->db->trans_start();
		
		$this->db->insert($tablename, $data);

		// $this->db->trans_complete();

		// $this->db->db_debug = TRUE;

		if($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}else{
			$this->db->trans_commit();
			return TRUE;
		}
	}
	function insert_get_lastid($tablename,$data=array()){	
		$this->db->insert($tablename, $data);
		if($this->db->trans_status() === FALSE){
			return FALSE;
		}else{
			$lastid=$this->db->insert_id();
			return $lastid ;
		}
	}
	function get_single_spek($tablename,$select,$option=array()){
		$this->db->select($select);
		$this->db->where($option);
		$this->db->limit(1);

		$query = $this->db->get($tablename);
		if($query->num_rows() == 1){
			return $query->row();
		}else{
			return FALSE;
		}
	}
	public function get_all_spek($tablename,$select,$order=null,$tipeorder=null,$option=array())
	{
		$this->db->select($select);
		$this->db->where($option);
		if(!empty($order)){
			$this->db->order_by($order,$tipeorder);
		}

		$query = $this->db->get($tablename);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	public function get_all_spek_j1($tablename,$select,$join_tb,$join_on,$join_type,$order=null,$tipeorder=null,$option=array())
	{
		$this->db->select($select);
		$this->db->join($join_tb,$join_on,$join_type);
		$this->db->where($option);
		if(!empty($order)){
			$this->db->order_by($order,$tipeorder);
		}

		$query = $this->db->get($tablename);
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	public function edit($tablename,$option=array(),$data)
	{
		$this->db->where($option);
		$this->db->update($tablename,$data);

		if($this->db->affected_rows() == 1){
			return true;
		}else{
			return false;
		}
	}
	public function count($tablename,$option=array())
	{
		$this->db->select('COUNT(*) as count');
		$this->db->where($option);

		$query = $this->db->get($tablename);
		if($query->num_rows() > 0){
			return $query->row()->count;
		}else{
			return 0;
		}
	}
	public function delete($tablename,$option=array())
	{
		$this->db->where($option);
		$this->db->delete($tablename); 
		if($this->db->affected_rows() == 1){
			return true;
		}else{
			return false;
		}
	}
}