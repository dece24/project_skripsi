<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class index_front extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->config->load('custom_config');
		$this->title = $this->config->item('title');
		$this->icon = $this->config->item('icon');

		$this->load->model('m_general');
	}
	
	public function index()
	{
		$data['title'] = $this->title;
		$data['nama_web'] = $this->title;
		$data['wall_1'] = $this->config->item('wall_1');
		$data['icon'] = $this->icon;

		$this->load->view('v_index',$data);
	}

	public function regist_user()
	{
		$data['title'] = $this->title;
		$data['nama_web'] = $this->title;
		$data['wall_1'] = $this->config->item('wall_1');
		$data['icon'] = $this->icon;

		$this->load->view('v_index_regist',$data);
	}

	public function regist_process(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('namalengkap','nama lengkap','required');
		$this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[tb_user.email_user]');
		$this->form_validation->set_rules('username', 'username', 'required|is_unique[tb_user.username_user]');
		$this->form_validation->set_rules('password', 'password', 'required|min_length[6]');
		$this->form_validation->set_rules('repassword', 'repassword', 'required|matches[password]');

		if ($this->form_validation->run() == FALSE){
			$data['title'] = $this->title;
			$data['nama_web'] = $this->title;
			$data['wall_1'] = $this->config->item('wall_1');
			$data['icon'] = $this->icon;
            $this->load->view('v_index_regist',$data);
        }else{
        	$username = $this->input->post('username');
        	$nama = $this->input->post('namalengkap');
            $email = $this->input->post('email');
            $password = sha1($this->input->post('password'));

            $data = array(
            		'nama' => $nama,
            		'username_user' => $username,
            		'email_user' => $email,
            		'password' => $password
            	);
            $input = $this->m_general->insert_transact('tb_user',$data);
            if($input === TRUE){
            	$this->session->set_flashdata('msg', 'registrasi berhasil');
            	redirect(base_url());
            }
        }
	}

	public function LoginFront(){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('param1','username / email','trim|required|callback_CheckUser');
		$this->form_validation->set_rules('password','password','trim|required');

		if($this->form_validation->run() == FALSE){
			$data['title'] = $this->title;
			$data['nama_web'] = $this->title;
			$data['wall_1'] = $this->config->item('wall_1');
			$data['icon'] = $this->icon;

            $this->load->view('v_index',$data);
		}else{
			redirect('dashboardUser');
		}
	}

	public function CheckUser($str){
		$password = $this->input->post('password');
		
		if(empty($str) || empty($password)){
			return TRUE;
		}
		$options = array(
						'username_user = "'.$str.'" OR email_user = "'.$str.'" ' => null,
						'password' => sha1($password),
					);
		$get_user = $this->m_general->get_single_spek('tb_user','id_user,username_user,is_active',$options);
		if(empty($get_user)){
			$this->form_validation->set_message('CheckUser','username/password salah');
			return false;
		}
		if($get_user->is_active == 0){
			$this->form_validation->set_message('CheckUser','account anda belum aktif');
			return false;
		}
		$data = array(
			'idUser' => $get_user->id_user,
			'usernameUser' => $get_user->username_user
		);
		$this->session->set_userdata($data);
		return TRUE;
	}

	public function LogoutFront(){
		$this->session->unset_userdata('idUser');
		$this->session->unset_userdata('usernameUser');

		redirect(base_url(),'refresh');
	}
}
