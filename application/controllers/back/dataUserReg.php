<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dataUserReg extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->config->load('custom_config');
		$this->title = $this->config->item('title');
		$this->icon = $this->config->item('icon');
		$this->load->library('Pengecekan');
		$this->pengecekan->AdminCheck();

		$this->load->model('m_general');
	}

	public function index()
	{
		$data['title'] = $this->title;
		$data['nama_web'] = $this->title;
		$data['page_title'] = 'Data Registrasi User';

		$data['wall_1'] = $this->config->item('wall_1');
		$data['icon'] = $this->icon;
		
		$select = '*';
		$order = 'date_created';
		$tipeorder = 'ASC';
		$option = array(
			'id_user is not null'=>null
		);
		$data['dataUser'] = $this->m_general->get_all_spek('tb_user',$select,$order,$tipeorder,$option);

		$this->load->view('back/v_back_dataUser',$data);
	}
	public function EditUserReg($id)
	{
		$data['title'] = $this->title;
		$data['nama_web'] = $this->title;
		$data['page_title'] = 'Edit Registrasi User';

		$data['wall_1'] = $this->config->item('wall_1');
		$data['icon'] = $this->icon;

		$select = '*';
		$option = array(
			'id_user'=>$id
		);
		$data['dataUser'] = $this->m_general->get_single_spek('tb_user',$select,$option);

		$this->load->view('back/v_back_editUser',$data);
	}

	public function UpdateUserReg($id)
	{
		$is_active = $this->input->post('is_active');

		$option = array(
			'id_user'=> $id
		);
		$data = array(
			'is_active' => $is_active
		);
		$update_user = $this->m_general->edit('tb_user',$option,$data);
		if($update_user == TRUE){
			$this->session->set_flashdata('msg', 'Data Updated');
            redirect(base_url().'admDataUser');
		}else{
			$this->session->set_flashdata('msg', 'Update Failed');
			 redirect(base_url().'admDataUser');
		}
	}
}
