<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class adminKTP extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->config->load('custom_config');
		$this->title = $this->config->item('title');
		$this->icon = $this->config->item('icon');
		$this->load->library('Pengecekan');
		$this->pengecekan->AdminCheck();

		$this->load->model('m_general');
	}

	public function index()
	{
		$data['title'] = $this->title;
		$data['nama_web'] = $this->title;
		$data['wall_1'] = $this->config->item('wall_1');
		$data['icon'] = $this->icon;
		$data['page_title'] = 'Data Permohonan KTP';
		$tablename = 'tb_reqktp';
		$select = '*';
		$join_tb = 'tb_user';
		$join_on = 'tb_user.id_user = tb_reqktp.id_user';
		$join_type = 'INNER JOIN';
		$order_by = 'datecreated';
		$tipeorder = 'desc';
		$option = array(
				'id_reqktp is not null'=>null
			);
		$data['get_all_data'] = $this->m_general->get_all_spek_j1($tablename,$select,$join_tb,$join_on,$join_type,$order_by,$tipeorder,$option);
		
		$this->load->view('back/v_back_listktp',$data);
	}
	public function updateStatus($id){
		$status = $this->input->post('statuspermohonan');
		if(empty($status)){
			redirect(base_url().'admListKTP');
		}
		$update_ktp = $this->m_general->edit('tb_reqktp',array('id_reqktp'=>$id),array('status_permohonan'=>$status));
		if($status == 'SELESAI'){
			//kirim email
		}
		if($status == 'DITOLAK'){
			$alasan = $this->input->post('alasantolak');
			$update_kk = $this->m_general->edit('tb_reqktp',array('id_reqktp'=>$id),array('alasan'=>$alasan));
		}else{
			$alasan = NULL;
			$update_kk = $this->m_general->edit('tb_reqktp',array('id_reqktp'=>$id),array('alasan'=>$alasan));
		}
		$this->session->set_flashdata('msg', 'Data Updated');
   		redirect(base_url().'admListKTP');
	}
	public function getDetail(){
		$id = $this->input->post('data_send');
		$get_data_ktp = $this->m_general->get_single_spek('tb_reqktp','*',array('id_reqktp'=>$id));
		if(empty($get_data_ktp)){
			echo 'data not found';
			return false;
		}
		echo form_open(base_url('admUpdateKTP/'.$get_data_ktp->id_reqktp));

		echo '<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title">DETAIL KTP</h4>
      		</div>';

  		echo '<div class="modal-body" style="overflow:hidden;">';
			echo '
					<div class="col-md-12">
					  	<div class="form-group">
						    <label for="">Status permohonan</label>
						    <select class="form-control" name="statuspermohonan" id="statuspermohonan">';
						    	$selected = '';
						    	echo '<option value="">--pilih--</option>';
						    		if($get_data_ktp->status_permohonan == 'REQ'){ $selected = 'selected="selected"'; }else{ $selected = ''; }
						    	echo '<option '.$selected.' value="REQ">REQ</option>';
						    		if($get_data_ktp->status_permohonan == 'PROSES'){ $selected = 'selected="selected"'; }else{ $selected = ''; }
						    	echo '<option '.$selected.'>PROSES</option>';
						    		if($get_data_ktp->status_permohonan == 'DITOLAK'){ $selected = 'selected="selected"'; }else{ $selected = ''; }
						    	echo '<option '.$selected.'>DITOLAK</option>';
						    		if($get_data_ktp->status_permohonan == 'SELESAI'){ $selected = 'selected="selected"'; }else{ $selected = ''; }
						    	echo '<option '.$selected.'>SELESAI</option>';
			echo			'</select>
							<textarea name="alasantolak" class="form-control hidden" placeholder="alasan penolakan" id="alasantolak"></textarea>
							<input type="submit" class="btn btn-primary" value="Save Status">
		  				</div>
	  				</div>';

			echo	'<div class="col-md-4">
					  	<div class="form-group">
						    <label for="">Nama Lengkap</label>
						    <input type="text" class="form-control" disabled="disabled" value="'.$get_data_ktp->namalengkap.'">
		  				</div>
	  				</div>
	  				<div class="col-md-4">
					  	<div class="form-group">
						    <label for="">NIK</label>
						    <input type="text" class="form-control"  disabled="disabled" value="'.$get_data_ktp->nik.'">
		  				</div>
	  				</div>
	  				<div class="col-md-4" style="float:right">
					  	<div class="form-group">
						    <label for="">Foto</label>
						    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							  <div class="panel panel-default">
							    <div class="panel-heading" role="tab" id="headingOne">
							      <h4 class="panel-title">
							        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="label label-success">
							          Foto Diri
							        </a>
							      </h4>
							    </div>
							    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							      <div class="panel-body">
							      	<a href="'.base_url().'assets/upload/fotodiri/'.$get_data_ktp->fotoktp.'" target="_blank" class="outlink">
							      		<i class="fa fa-external-link" aria-hidden="true"></i>
							      	</a>
							        <img src="'.base_url().'assets/upload/fotodiri/'.$get_data_ktp->fotoktp.'" class="img-responsive">
							      </div>
							    </div>
							  </div>
							  <div class="panel panel-default">
							    <div class="panel-heading" role="tab" id="headingTwo">
							      <h4 class="panel-title">
							        <a class="label label-success collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							          Foto KK
							        </a>
							      </h4>
							    </div>
							    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							      <div class="panel-body">
							      	<a href="'.base_url().'assets/upload/kk_ktp/'.$get_data_ktp->fotokk.'" target="_blank" class="outlink">
							      		<i class="fa fa-external-link" aria-hidden="true"></i>
							      	</a>
							        <img src="'.base_url().'assets/upload/kk_ktp/'.$get_data_ktp->fotokk.'" class="img-responsive">
							      </div>
							    </div>
							  </div>
							</div>
		  				</div>
	  				</div>
	  				<div class="col-md-4">
					  	<div class="form-group">
						    <label for="">Tipe Permohonan</label>
						    <input type="text" class="form-control"  disabled="disabled" value="'.strtoupper($get_data_ktp->tipektp).'">
		  				</div>
	  				</div>
	  				<div class="col-md-4">
					  	<div class="form-group">
						    <label for="">No KK</label>
						    <input type="text" class="form-control"  disabled="disabled" value="'.$get_data_ktp->nokk.'">
		  				</div>
	  				</div>
	  				<div class="col-md-8">
					  	<div class="form-group">
						    <label for="">Daerah Administrasi</label>
						    <textarea class="form-control" disabled="disabled" style="min-height:115px;">'
						    	.$get_data_ktp->nama_prop.' - '
						    	.$get_data_ktp->nama_kota.'&#13;'
						    	.$get_data_ktp->nama_kel.'&#13;'
						    	.$get_data_ktp->nama_kec.'&#13;'.
						    '</textarea>
		  				</div>
	  				</div>
	  				<div class="col-md-8">
					  	<div class="form-group">
						    <label for="">Alamat</label>
						    <textarea class="form-control" disabled="disabled" style="min-height:100px">'.$get_data_ktp->alamat.'</textarea>
						    <div class="col-md-4" style="padding:0px;">
						    	<input type="text" class="form-control"  disabled="disabled" value="RT/RT 0'.$get_data_ktp->nort.' / 0'.$get_data_ktp->norw.'">
						    </div>
						    <div class="col-md-4" style="padding:0px;">
						    	<input type="text" class="form-control"  disabled="disabled" value="Kodepos '.$get_data_ktp->kodepos.'">
						    </div>
		  				</div>
	  				</div>
	  				
				';
		echo '</div>';

		echo '<div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      	</div>';

      	echo '</form>';
	}
}
