<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class adminKK extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->config->load('custom_config');
		$this->title = $this->config->item('title');
		$this->icon = $this->config->item('icon');
		$this->load->library('Pengecekan');
		$this->pengecekan->AdminCheck();

		$this->load->model('m_general');
	}

	public function index()
	{
		$data['title'] = $this->title;
		$data['nama_web'] = $this->title;
		$data['wall_1'] = $this->config->item('wall_1');
		$data['icon'] = $this->icon;
		$data['page_title'] = 'Data Permohonan KK';
		$tablename = 'tb_reqkk';
		$select = '*';
		$join_tb = 'tb_user';
		$join_on = 'tb_user.id_user = tb_reqkk.id_user';
		$join_type = 'INNER JOIN';
		$order_by = 'datecreated';
		$tipeorder = 'desc';
		$option = array(
				'id_reqkk is not null'=>null
			);
		$data['get_all_data'] = $this->m_general->get_all_spek_j1($tablename,$select,$join_tb,$join_on,$join_type,$order_by,$tipeorder,$option);
		
		$this->load->view('back/v_back_listkk',$data);
	}
	public function updateStatus($id){
		$status = $this->input->post('statuspermohonan');
		if(empty($status)){
			redirect(base_url().'admListKK');
		}
		$update_kk = $this->m_general->edit('tb_reqkk',array('id_reqkk'=>$id),array('status_permohonan'=>$status));
		if($status == 'SELESAI'){
			//kirim email
		}
		if($status == 'DITOLAK'){
			$alasan = $this->input->post('alasantolak');
			$update_kk = $this->m_general->edit('tb_reqkk',array('id_reqkk'=>$id),array('alasan'=>$alasan));
		}else{
			$alasan = NULL;
			$update_kk = $this->m_general->edit('tb_reqkk',array('id_reqkk'=>$id),array('alasan'=>$alasan));
		}
		$this->session->set_flashdata('msg', 'Data Updated');
   		redirect(base_url().'admListKK');
	}
	public function getDetail(){
		$id = $this->input->post('data_send');
		$get_data_kk = $this->m_general->get_single_spek('tb_reqkk','*',array('id_reqkk'=>$id));
		$get_detail_kk = $this->m_general->get_all_spek('tb_reqkk_detail','*','','',array('id_reqkk'=>$id));
		if(empty($get_data_kk)){
			echo 'data not found';
			return false;
		}
		echo form_open(base_url('admUpdateKK/'.$get_data_kk->id_reqkk));

		echo '<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title">DETAIL KTP</h4>
      		</div>';

  		echo '<div class="modal-body" style="overflow:hidden;">';
			echo '
					<div class="col-md-12">
					  	<div class="form-group">
						    <label for="">Status permohonan</label>
						    <select class="form-control" name="statuspermohonan" id="statuspermohonan">';
						    	$selected = '';
						    	echo '<option value="">--pilih--</option>';
						    		if($get_data_kk->status_permohonan == 'REQ'){ $selected = 'selected="selected"'; }else{ $selected = ''; }
						    	echo '<option '.$selected.' value="REQ">REQ</option>';
						    		if($get_data_kk->status_permohonan == 'PROSES'){ $selected = 'selected="selected"'; }else{ $selected = ''; }
						    	echo '<option '.$selected.'>PROSES</option>';
						    		if($get_data_kk->status_permohonan == 'DITOLAK'){ $selected = 'selected="selected"'; }else{ $selected = ''; }
						    	echo '<option '.$selected.'>DITOLAK</option>';
						    		if($get_data_kk->status_permohonan == 'SELESAI'){ $selected = 'selected="selected"'; }else{ $selected = ''; }
						    	echo '<option '.$selected.'>SELESAI</option>';
			echo			'</select>
							<textarea name="alasantolak" class="form-control hidden" placeholder="alasan penolakan" id="alasantolak"></textarea>
							<input type="submit" class="btn btn-primary" value="Save Status">
		  				</div>
	  				</div>';

			echo	'<div class="col-md-8">
					  	<div class="form-group">
						    <label for="">Nama Kepala Keluarga</label>
						    <input type="text" class="form-control" disabled="disabled" value="'.$get_data_kk->nama_kepkel.'">
		  				</div>
	  				</div>
	  				<div class="col-md-4" style="float:right">
					  	<div class="form-group">
						    <label for="">Foto</label>
						    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							  <div class="panel panel-default">
							    <div class="panel-heading" role="tab" id="headingOne">
							      <h4 class="panel-title">
							        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="label label-success">
							          Foto KK
							        </a>
							      </h4>
							    </div>
							    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							      <div class="panel-body">
							      	<a href="'.base_url().'assets/upload/kk_aja/'.$get_data_kk->fotokk.'" target="_blank" class="outlink">
							      		<i class="fa fa-external-link" aria-hidden="true"></i>
							      	</a>
							        <img src="'.base_url().'assets/upload/kk_aja/'.$get_data_kk->fotokk.'" class="img-responsive">
							      </div>
							    </div>
							  </div>
							</div>
		  				</div>
	  				</div>
	  				<div class="col-md-4">
					  	<div class="form-group">
						    <label for="">Daerah Administrasi</label>
						    <textarea class="form-control" disabled="disabled" style="min-height:115px;">'
						    	.$get_data_kk->nama_prop.' - '
						    	.$get_data_kk->nama_kota.'&#13;'
						    	.$get_data_kk->nama_kel.'&#13;'
						    	.$get_data_kk->nama_kec.'&#13;'.
						    '</textarea>
		  				</div>
	  				</div>
	  				<div class="col-md-4">
					  	<div class="form-group">
						    <label for="">Alamat</label>
						    <textarea class="form-control" disabled="disabled" style="min-height:100px">'.$get_data_kk->alamat.'</textarea>
						    <div class="col-md-4" style="padding:0px;">
						    	<input type="text" class="form-control"  disabled="disabled" value="RT/RT 0'.$get_data_kk->nort.' / 0'.$get_data_kk->norw.'">
						    </div>
						    <div class="col-md-8" style="padding:0px;">
						    	<input type="text" class="form-control"  disabled="disabled" value="Kodepos '.$get_data_kk->kodepos.'">
						    </div>
		  				</div>
	  				</div>
	  				
				';
		echo '</div>';
		echo '<div class="container">';
			echo '<table class="table table-bordered">';
				echo '<thead>
						<tr>
							<th class="text-center">Nama <br> NIK </th>
							<th class="text-center">Jenis Kelamin</th>
							<th class="text-center">Agama <br> Kewarganegaraan</th>
							<th class="text-center">Pendidikan <br> Pekerjaan</th>
							<th class="text-center">Tgl.Lahir</th>
							<th class="text-center">Status Per.</th>
							<th class="text-center">Hubungan</th>
							<th class="text-center">Ayah - Ibu</th>
							<th class="text-center">Passport <br> KITAS </th>
						</tr>
					</thead>';
				foreach ($get_detail_kk as $key => $value) {
					echo '<tr>';
						echo '<td class="text-center">'.$value->nama.' <br> ('.$value->nik.')</td>';
						echo '<td class="text-center">'.$value->jenis_kelamin.'</td>';
						echo '<td class="text-center">'.$value->agama.' <br> '.$value->kwn.'</td>';
						echo '<td class="text-center">'.$value->pendidikan.' <br> ('.$value->pekerjaan.')</td>';
						echo '<td class="text-center">'.date("d-m-Y", strtotime($value->tanggal_lahir)).'</td>';
						echo '<td class="text-center">'.$value->status.'</td>';
						echo '<td class="text-center">'.$value->hubungan.'</td>';
						echo '<td class="text-center">'.$value->ayah.' - '.$value->ibu.'</td>';
						echo '<td class="text-center">'.$value->passport.' <br> '.$value->kitas.'</td>';
					echo '</tr>';
				}
			echo '<table>';
		echo '</div>';

		echo '<div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      	</div>';

      	echo '</form>';
	}
}
