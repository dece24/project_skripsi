<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class homeAdmin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->config->load('custom_config');
		$this->title = $this->config->item('title');
		$this->icon = $this->config->item('icon');
		$this->load->library('Pengecekan');
		$this->pengecekan->AdminCheck();

		$this->load->model('m_general');
	}

	public function index()
	{
		$data['title'] = $this->title;
		$data['nama_web'] = $this->title;
		$data['wall_1'] = $this->config->item('wall_1');
		$data['icon'] = $this->icon;

		$data['count_user'] = $this->m_general->count('tb_user',array('id_user is not null'=>null));
		$data['count_req_ktp'] = $this->m_general->count('tb_reqktp',array('id_reqktp is not null'=>null));
		$data['count_req_kk'] = $this->m_general->count('tb_reqkk',array('id_reqkk is not null'=>null));
		
		$this->load->view('back/v_back_dashboard',$data);
	}
}
