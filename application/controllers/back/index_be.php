<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class index_be extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->config->load('custom_config');
		$this->title = $this->config->item('title');
		$this->icon = $this->config->item('icon');

		$this->load->model('m_general');
	}

	public function index()
	{
		$data['title'] = $this->title;
		$data['nama_web'] = $this->title;
		$data['wall_1'] = $this->config->item('wall_1');
		$data['icon'] = $this->icon;

		$this->load->view('back/v_back_index',$data);
	}

	public function LoginBack()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'username', 'trim|required|callback_checkAdmin');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		if ($this->form_validation->run() == FALSE){
			$data['title'] = $this->title;
			$data['nama_web'] = $this->title;
			$data['wall_1'] = $this->config->item('wall_1');
			$data['icon'] = $this->icon;
            $this->load->view('back/v_back_index',$data);
		}else{
			redirect('dashboardAdmin');
		}
	}

	public function checkAdmin($str)
	{
		$password = $this->input->post('password');
		if(empty($str) || empty($password)){
			return TRUE;
		}
		$options = array(
						'username_admin' => $str,
						'password_admin' => sha1($password),
						'admin_active' => 1
					);
		$get_admin = $this->m_general->get_single_spek('tb_admin','id_admin,username_admin',$options);
		if(empty($get_admin)){
			$this->form_validation->set_message('checkAdmin','username/password salah');
			return FALSE;
		}else{
			$data = array(
					'idAdm' => $get_admin->id_admin,
					'usernameAdm' => $get_admin->username_admin
				);
			$this->session->set_userdata($data);
			return TRUE;
		}
	}

	public function LogoutBack()
	{
		$this->session->unset_userdata('idAdm');
		$this->session->unset_userdata('usernameAdm');

		redirect('back');
	}
}
