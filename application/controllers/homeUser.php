<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class homeUser extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->config->load('custom_config');
		$this->title = $this->config->item('title');
		$this->icon = $this->config->item('icon');
		$this->load->library('Pengecekan');
		$this->pengecekan->UserCheck();
		$this->id_user = $this->session->userdata('idUser');

		$this->load->model('m_general');
	}

	public function index()
	{
		$data['title'] = $this->title;
		$data['nama_web'] = $this->title;
		$data['wall_1'] = $this->config->item('wall_1');
		$data['icon'] = $this->icon;
		$data['count_req_kk'] = $this->m_general->count('tb_reqkk',array('id_user'=>$this->id_user));
		$data['count_req_ktp'] = $this->m_general->count('tb_reqktp',array('id_user'=>$this->id_user));
		
		$this->load->view('v_front_dashboard',$data);
	}
}
