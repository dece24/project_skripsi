<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class userKTP extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->config->load('custom_config');
		$this->title = $this->config->item('title');
		$this->icon = $this->config->item('icon');
		$this->load->library('Pengecekan');
		$this->pengecekan->UserCheck();

		$this->id_user = $this->session->userdata('idUser');

		$this->load->model('m_general');
	}

	public function index()
	{
		$data['title'] = $this->title;
		$data['nama_web'] = $this->title;
		$data['wall_1'] = $this->config->item('wall_1');
		$data['icon'] = $this->icon;
		$data['page_title'] = 'Form Permohonan Kartu Tanda Penduduk';

		$data['data_prop'] = $this->m_general->get_all_spek('m_prop','*','','',array('id_prop is not null'=>null));
		$data['data_kota'] = $this->m_general->get_all_spek('m_kota','*','','',array('id_kota is not null'=>null));
		$data['data_kec'] = $this->m_general->get_all_spek('m_kec','*','','',array('id_kec is not null'=>null));
		$data['data_kel'] = $this->m_general->get_all_spek('m_kel','*','','',array('id_kel is not null'=>null));
		$data['data_meng'] = $this->m_general->get_single_spek('m_mengetahui','*',array('id_meng is not null'=>null));
		
		$this->load->view('v_req_ktp',$data);
	}
	
	public function userKTPProses()
	{
		$this->load->library('upload');
        $config['upload_path'] = './assets/upload/fotodiri/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '2048';

        $new_name = 'ktp'.$this->id_user.time();
		$config['file_name'] = $new_name;

        $this->upload->initialize($config);
        if($_FILES['fotodiri']['name']){
            if($this->upload->do_upload('fotodiri')){
			    $fotoktp = $this->upload->file_name;;
            }else{
				$fotoktp = 'failed upload';
			}
        }
        
        $this->load->library('upload');
        $config['upload_path'] = './assets/upload/kk_ktp/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '2048';

    	$new_name = 'kk'.$this->id_user.time();
		$config['file_name'] = $new_name;

        $this->upload->initialize($config);
        if($_FILES['fotokk']['name']){
            if($this->upload->do_upload('fotokk')){
                $fotokk = $this->upload->file_name;;
            }else{
				$fotokk = 'failed upload';
			}
        }

        $data = array(
				'id_user' => $this->session->userdata('idUser'),
				'nama_prop' => $this->input->post('propselect'),
				'status_permohonan' => 'REQ',
				'nama_kota' => $this->input->post('kotaselect'),
				'nama_kec' => $this->input->post('kecselect'),
				'nama_kel' => $this->input->post('kelselect'),
				'tipektp' => $this->input->post('tipektp'),
				'namalengkap' => $this->input->post('namalengkap'),
				'nokk' => $this->input->post('nokk'),
				'nik' => $this->input->post('nik'),
				'alamat' => $this->input->post('alamat'),
				'nort' => $this->input->post('nort'),
				'norw' => $this->input->post('norw'),
				'kodepos' => $this->input->post('kodepos'),
				'nama_meng' => $this->input->post('mengnama'),
				'nik_meng' => $this->input->post('mengnik'),
				'fotoktp' => $fotoktp,
				'fotokk' => $fotokk
			);
		$this->m_general->insert_transact('tb_reqktp',$data);

		$this->session->set_flashdata('msg', 'Data Berhasil Ditambahkan');
   	   	redirect(base_url().'listKTP');
	}

	public function listReqKTP()
	{
		$data['title'] = $this->title;
		$data['nama_web'] = $this->title;
		$data['wall_1'] = $this->config->item('wall_1');
		$data['icon'] = $this->icon;
		$data['page_title'] = 'List Permohonan Kartu Tanda Penduduk';

		$data['all_data'] = $this->m_general->get_all_spek('tb_reqktp','*','','',array('id_user'=>$this->id_user));

		$this->load->view('v_list_ktp',$data);
	}
	
	public function hapusReqKTP($id)
	{
		$cek = $this->m_general->delete('tb_reqktp',array('id_reqktp'=>$id,'id_user'=>$this->id_user));
		if($cek == TRUE){
			$this->session->set_flashdata('msg', 'Data Berhasil Dihapus');
		}else{
			$this->session->set_flashdata('msg', 'Data Gagal Dihapus');
		}
		
   	   	redirect(base_url().'listKTP');
	}

	public function ktpFotoDiri()
	{
		$id = $this->input->post('data_send');
		$option = array('id_reqktp'=>$id,'id_user'=>$this->id_user);

		$get_data = $this->m_general->get_single_spek('tb_reqktp','*',$option);
		if(empty($get_data)){
			echo 'data not found';
			return false;
		}
		echo '<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title">Foto Diri</h4>
      		</div>';

      	echo '<div class="modal-body">';
        		echo '<div class="con-foto-ktp">';
        			echo '<a href="'.base_url().'assets/upload/fotodiri/'.$get_data->fotoktp.'" target="_blank" class="link-foto"><i class="fa fa-external-link" aria-hidden="true"></i></a>';
        			echo '<img src="'.base_url().'assets/upload/fotodiri/'.$get_data->fotoktp.'" alt="'.$get_data->namalengkap.'">';
        		echo '</div>';	
      	echo '</div>';
      
      	echo '<div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      	</div>';
	}
	
	public function ktpFotoKK()
	{
		$id = $this->input->post('data_send');
		$option = array('id_reqktp'=>$id,'id_user'=>$this->id_user);

		$get_data = $this->m_general->get_single_spek('tb_reqktp','*',$option);
		if(empty($get_data)){
			echo 'data not found';
			return false;
		}
		echo '<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title">Foto Diri</h4>
      		</div>';

      	echo '<div class="modal-body">';
        		echo '<div class="con-foto-ktp">';
        			echo '<a href="'.base_url().'assets/upload/kk_ktp/'.$get_data->fotokk.'" target="_blank" class="link-foto"><i class="fa fa-external-link" aria-hidden="true"></i></a>';
        			echo '<img src="'.base_url().'assets/upload/kk_ktp/'.$get_data->fotokk.'" alt="'.$get_data->namalengkap.'">';
        		echo '</div>';	
      	echo '</div>';
      
      	echo '<div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      	</div>';
	}
}
