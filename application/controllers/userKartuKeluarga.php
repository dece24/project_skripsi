<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class userKartuKeluarga extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->config->load('custom_config');
		$this->title = $this->config->item('title');
		$this->icon = $this->config->item('icon');
		$this->load->library('Pengecekan');
		$this->pengecekan->UserCheck();
		$this->id_user = $this->session->userdata('idUser');

		$this->load->model('m_general');
	}

	public function index()
	{
		$data['title'] = $this->title;
		$data['nama_web'] = $this->title;
		$data['wall_1'] = $this->config->item('wall_1');
		$data['icon'] = $this->icon;
		$data['page_title'] = 'Form Permohonan Kartu Keluarga';
		$data['data_prop'] = $this->m_general->get_all_spek('m_prop','*','','',array('id_prop is not null'=>null));
		$data['data_kota'] = $this->m_general->get_all_spek('m_kota','*','','',array('id_kota is not null'=>null));
		$data['data_kec'] = $this->m_general->get_all_spek('m_kec','*','','',array('id_kec is not null'=>null));
		$data['data_kel'] = $this->m_general->get_all_spek('m_kel','*','','',array('id_kel is not null'=>null));
		
		$this->load->view('v_req_kk',$data);
	}
	public function userKKProses(){
		$this->load->library('upload');
        $config['upload_path'] = './assets/upload/kk_aja/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '2048';
        $new_name = 'kk_aja'.$this->id_user.time();
		$config['file_name'] = $new_name;

        $this->upload->initialize($config);
        if($_FILES['fotokk']['name']){
            if($this->upload->do_upload('fotokk')){
			    $fotokk = $this->upload->file_name;;
            }else{
				$fotokk = 'failed upload';
			}
        }

        $data = array(
			'id_user' => $this->id_user,
			'nama_prop' => $this->input->post('propselect'),
			'status_permohonan' => 'REQ',
			'nama_kota' => $this->input->post('kotaselect'),
			'nama_kec' => $this->input->post('kecselect'),
			'nama_kel' => $this->input->post('kelselect'),
			'nama_kepkel' => $this->input->post('namakepkel'),
			'alamat' => $this->input->post('alamat'),
			'nort' => $this->input->post('nort'),
			'norw' => $this->input->post('norw'),
			'kodepos' => $this->input->post('kodepos'),
			'fotokk' => $fotokk
		);
		$latestid = $this->m_general->insert_get_lastid('tb_reqkk',$data);
        // $latestid = '1';

        $namainput = $this->input->post('namainput');
        $agama = $this->input->post('agama');
        $pekerjaan = $this->input->post('pekerjaan');
        $ayah = $this->input->post('namaayah');
        $ibu = $this->input->post('namaibu');
        $nik = $this->input->post('nik');
        $pendidikan = $this->input->post('pendidikan');
        $status = $this->input->post('status');
        $kwn = $this->input->post('kwn');
        $kelamin = $this->input->post('kelamin');
        $tanggal = $this->input->post('tanggal');
        $hubungan = $this->input->post('hubungan');
        $passport = $this->input->post('passport');
        $kitas = $this->input->post('kitas');
        foreach ($nik as $key => $value) {
        	if(!empty($value)){
        		$data_detail = array(
        			'id_reqkk' => $latestid,
        			'nama' => $namainput[$key],
        			'nik' => $value,
        			'jenis_kelamin' => $kelamin[$key],
        			'agama' => $agama[$key],
        			'pendidikan' => $pendidikan[$key],
        			'tanggal_lahir' => $tanggal[$key],
        			'pekerjaan' => $pekerjaan[$key],
        			'status' => $status[$key],
        			'hubungan' => $hubungan[$key],
        			'ayah' => $ayah[$key],
        			'ibu' => $ibu[$key],
        			'kwn' => $kwn[$key],
        			'passport' => $passport[$key],
        			'kitas' => $kitas[$key]
        		);
        		$this->m_general->insert_transact('tb_reqkk_detail',$data_detail);
        	}else{
        	}
        }
        $this->session->set_flashdata('msg', 'Data Berhasil Ditambahkan');
   	   	redirect(base_url().'listKK');
	}

	public function listReqKK()
	{
		$data['title'] = $this->title;
		$data['nama_web'] = $this->title;
		$data['wall_1'] = $this->config->item('wall_1');
		$data['icon'] = $this->icon;
		$data['page_title'] = 'List Permohonan Kartu Keluarga (KK)';

		$data['all_data'] = $this->m_general->get_all_spek('tb_reqkk','*','','',array('id_user'=>$this->id_user));

		$this->load->view('v_list_kk',$data);
	}

	public function hapusReqKK($id){
		$cek = $this->m_general->delete('tb_reqkk',array('id_reqkk'=>$id,'id_user'=>$this->id_user));
		$this->m_general->delete('tb_reqkk_detail',array('id_reqkk'=>$id));
		if($cek == TRUE){
			$this->session->set_flashdata('msg', 'Data Berhasil Dihapus');
		}else{
			$this->session->set_flashdata('msg', 'Data Gagal Dihapus');
		}
		
   	   	redirect(base_url().'listKK');
	}
	public function detailKK(){
		$id = $this->input->post('data_send');
		$option = array(
				'id_reqkk' => $id
			);
		$get_detail_kk = $this->m_general->get_all_spek('tb_reqkk_detail','*','','',$option);
		$get_data_kk = $this->m_general->get_single_spek('tb_reqkk','*',$option);
		if(empty($get_detail_kk) || empty($get_data_kk)){
			echo 'data not found';
			return false;
		}
		echo '<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title">Detail Kartu Keluarga Request</h4>
     		 </div>';
      	echo '<div class="modal-body">';
      			echo '<h4>Foto KK</h4>';
        		echo '<div class="con-foto">';
        			echo '<a href="'.base_url().'assets/upload/kk_aja/'.$get_data_kk->fotokk.'" target="_blank" class="link-foto"><i class="fa fa-external-link" aria-hidden="true"></i></a>';
        			echo '<img src="'.base_url().'assets/upload/kk_aja/'.$get_data_kk->fotokk.'" alt="'.$get_data_kk->nama_kepkel.'">';
        		echo '</div>';
      	echo '</div>';
      	echo '<div class="container">';
		echo '<table class="table table-bordered">';
			echo '<thead>
					<tr>
						<th class="text-center">Nama <br> NIK </th>
						<th class="text-center">Jenis Kelamin</th>
						<th class="text-center">Agama <br> Kewarganegaraan</th>
						<th class="text-center">Pendidikan <br> Pekerjaan</th>
						<th class="text-center">Tgl.Lahir</th>
						<th class="text-center">Status Per.</th>
						<th class="text-center">Hubungan</th>
						<th class="text-center">Ayah - Ibu</th>
						<th class="text-center">Passport <br> KITAS </th>
					</tr>
				</thead>';
			foreach ($get_detail_kk as $key => $value) {
				echo '<tr>';
					echo '<td class="text-center">'.$value->nama.' <br> ('.$value->nik.')</td>';
					echo '<td class="text-center">'.$value->jenis_kelamin.'</td>';
					echo '<td class="text-center">'.$value->agama.' <br> '.$value->kwn.'</td>';
					echo '<td class="text-center">'.$value->pendidikan.' <br> ('.$value->pekerjaan.')</td>';
					echo '<td class="text-center">'.date("d-m-Y", strtotime($value->tanggal_lahir)).'</td>';
					echo '<td class="text-center">'.$value->status.'</td>';
					echo '<td class="text-center">'.$value->hubungan.'</td>';
					echo '<td class="text-center">'.$value->ayah.' - '.$value->ibu.'</td>';
					echo '<td class="text-center">'.$value->passport.' <br> '.$value->kitas.'</td>';
				echo '</tr>';
			}
		echo '<table>';
		echo '</div>';


      	echo '<div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>';
	}
}
