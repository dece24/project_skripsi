<?php $this->load->view('general/v_header');?>
<body id="vtwo">
<?php $this->load->view('back/general_back/v_backmenu');?>
<div id="page-wrapper">
	<h2 class="b-sub-page-title"><?php echo strtoupper($page_title);?></h2>
	<hr class="b-hr"></hr>
	<?php
		if(empty($dataUser)){
			echo 'data not found';
			return false;
		}
	?>
	<!-- <form class="form-horizontal"> -->
	<?php echo form_open(base_url().'admUpdateUser/'.$dataUser->id_user,array('class'=>'form-horizontal'))?>
	 	<div class="form-group">
		    <label for="nama" class="col-sm-2 control-label">Nama</label>
		    <div class="col-sm-4">
		      	<input type="text" class="form-control" id="nama" value="<?php echo $dataUser->nama;?>" readonly="TRUE">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-2 control-label">Username</label>
		    <div class="col-sm-4">
		      	<input type="text" class="form-control" id="" value="<?php echo $dataUser->username_user;?>" readonly="TRUE">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-2 control-label">Email</label>
		    <div class="col-sm-4">
		      	<input type="text" class="form-control" id="" value="<?php echo $dataUser->email_user;?>" readonly="TRUE">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-2 control-label">Status Aktif</label>
		    <div class="col-sm-4">
		    	<select class="form-control" name="is_active">
		    		<option <?php if($dataUser->is_active == 1) { echo 'selected="selected"'; } ?> value="1">Aktif</option>
		    		<option <?php if($dataUser->is_active == 0) { echo 'selected="selected"'; } ?> value="0">Non-Aktif</option>
		    	</select>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-2 control-label"></label>
		    <div class="col-sm-4">
		    	<input type="submit" value="SAVE" class="btn btn-primary">
		    </div>
	  	</div>
	</form>
</div>
<?php $this->load->view('general/v_footer');?>