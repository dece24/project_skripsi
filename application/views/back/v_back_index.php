<?php $this->load->view('general/v_header');?>
<body id="vone">
<div class="container vi-header" style="background-image: url('<?php echo base_url().'assets/image/'.$wall_1;?>')">
  <a href="<?php echo base_url();?>">
    <img src="<?php echo base_url().'assets/image/'.$icon;?>" class="vi-h-img">
    <h5 class="vi-h-h5"><?php echo $nama_web; ?></h5>
  </a>
</div>
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto mx-auto-cos">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Admin Login</h5>
            <?php echo form_open('loginAdmin');?>
              <div class="form-group">
              	<label for="username">Username</label>
                <input type="text" id="username" class="form-control" placeholder="Username" name="username" autofocus>
                <?php echo form_error('username', '<div class="error">', '</div>'); ?>
              </div>
              <div class="form-group">
              	 <label for="inputPassword">Password</label>
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password">
                <?php echo form_error('password', '<div class="error">', '</div>'); ?>
              </div>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php $this->load->view('general/v_footer');?>
<script type="text/javascript">

</script>