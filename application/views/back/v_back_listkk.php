<?php $this->load->view('general/v_header');?>
<body id="vtwo">
<?php $this->load->view('back/general_back/v_backmenu');?>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-detail">
  <div class="modal-dialog modal-lg modal-lg-cos" role="document">
    <div class="modal-content">
      ...
    </div>
  </div>
</div>
<div id="page-wrapper">
	<h2 class="b-sub-page-title"><?php echo strtoupper($page_title);?></h2>
	<hr class="b-hr"></hr>

	<div class="table-wrapper">
		<table id="table_admin" class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Request By</th>
					<th class="text-center">Status</th>
					<th class="text-center">Date Created</th>
					
					<th class="text-center">Option</th>
				</tr>
			</thead>
			<tbody>
				<?php
					if(!empty($get_all_data)){
						foreach ($get_all_data as $key => $value) {
							?>
								<tr>
									<td class="text-center"><?php echo $key+1; ?></td>
									<td class="text-center"><?php echo $value->nama; ?><br><?php echo $value->username_user; ?></td>
									<td class="text-center">
										<?php
											echo '<span class="label label-primary">'.$value->status_permohonan.'</span>';
										?>
										<?php
											if(!empty($value->alasan)){
												echo '<br>';
												echo '<textarea class="form-control" disabled="disaabled">'.$value->alasan.'</textarea>';
											}
										?>
									</td>
									<td class="text-center"><?php echo $value->datecreated; ?></td>
									
									<td class="text-center">
										<?php
											echo '<a class="label label-warning btn-detail" id="'.$value->id_reqkk.'">Update</a>';
										?>
									</td>
								</tr>
							<?php
						}
					}else{
						?>
							<tr>
								<td colspan="max">Not Found</td>
							</tr>
						<?php
					}
				?>
			</tbody>
		</table>
	</div>
</div>
<?php $this->load->view('general/v_footer');?>
<script type="text/javascript">
	$('.btn-detail').on('click',function(){
		var id = $(this).attr('id');
		$.ajax({
	        url: '<?php echo base_url('adminDetailKK')?>',
	        type: 'post',
	        dataType: 'html',
	        data: {'data_send' : id},
	        beforeSend:function(){
	        	$('#modal-detail').modal('show');
	        	$('.modal-content').html('');
	        },
	        success: function(result) {
	        	$('.modal-content').html(result);
	        	$('#statuspermohonan').on('change',function(){
	        		var ini_stat = $(this).val();
	        		if(ini_stat == 'DITOLAK'){
	        			$('#alasantolak').removeClass('hidden');
	        		}else{
	        			$('#alasantolak').addClass('hidden');
	        		}
	        	})
	        },
	        error: function(jqXHR,textStatus,errorThrown) {
	          console.log(jqXHR+" | "+textStatus+" | "+errorThrown);
	          alert('Ajax Request Failed!');
	        }
	    });
	});
</script>