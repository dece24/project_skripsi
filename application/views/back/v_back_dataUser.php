<?php $this->load->view('general/v_header');?>
<body id="vtwo">
<?php $this->load->view('back/general_back/v_backmenu');?>
<div id="page-wrapper">
	<h2 class="b-sub-page-title"><?php echo strtoupper($page_title);?></h2>
	<hr class="b-hr"></hr>

	<div class="table-wrapper">
		<table id="table_admin" class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Nama | Username</th>
					<th class="text-center">Email</th>
					<th class="text-center">Date Created</th>
					<th class="text-center">Status</th>
					<th class="text-center">Option</th>
				</tr>
			</thead>
			<tbody>
				<?php
					if(!empty($dataUser)){
						foreach ($dataUser as $key => $value) {
							?>
								<tr>
									<td class="text-center"><?php echo $key+1; ?></td>
									<td class="text-center"><?php echo $value->nama; ?><br><?php echo $value->username_user; ?></td>
									<td class="text-center"><?php echo $value->email_user; ?></td>
									<td class="text-center"><?php echo $value->date_created; ?></td>
									<td class="text-center">
										<?php
											if($value->is_active == 0){
												echo '<span class="label label-warning">NON-ACTIVE</span>';
											}else{
												echo '<span class="label label-primary">ACTIVE</span>';
											}
										?>	
									</td>
									<td class="text-center">
										<a href="<?php echo base_url().'admEditUser/'.$value->id_user;?>">Edit</a>
									</td>
								</tr>
							<?php
						}
					}else{
						?>
							<tr>
								<td colspan="max">Not Found</td>
							</tr>
						<?php
					}
				?>
			</tbody>
		</table>
	</div>
</div>
<?php $this->load->view('general/v_footer');?>