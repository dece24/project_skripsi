<?php $this->load->view('general/v_header');?>
<body id="vthree">
<?php $this->load->view('general/v_frontmenu');?>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modal-detail">
  <div class="modal-dialog modal-lg modal-lg-cos" role="document">
    <div class="modal-content">
      ...
    </div>
  </div>
</div>
<div id="page-wrapper">
	<h2 class="b-sub-page-title"><?php echo strtoupper($page_title);?></h2>
	<hr class="b-hr"></hr>
	<table class="table table-bordered tb_datatables">
		<thead>
			<tr>
				<th class="text-center">No</th>
				<th class="text-center">Status Permohonan</th>
				<th class="text-center">Nama Kepala Keluarga</th>
				<th class="text-center">Alamat</th>
				<th class="text-center">Daerah Adm</th>
				<th class="text-center">Opsi</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if(empty($all_data)){
					echo '<tr><td>Data tidak ditemukan</td></tr>';
				}else{
					foreach ($all_data as $key => $value) {
						?>
							<tr>
								<td class="text-center"><?php echo $key+1; ?></td>
								<td class="text-center">
									<?php echo '<span class="label label-success">'.$value->status_permohonan.'</span>'; ?>
									<?php
										if(!empty($value->alasan)){
											echo '<br>';
											echo '<textarea class="form-control" disabled="disaabled">'.$value->alasan.'</textarea>';
										}
									?>
								</td>
								<td class="text-center"><?php echo $value->nama_kepkel; ?></td>
								<td class="text-center"><?php echo $value->alamat; ?><br><?php echo 'RT 0'.$value->nort.'/'.'RW 0'.$value->norw; ?></td>
								<td class="text-center">
									<?php echo $value->nama_prop; ?><br>
									<?php echo $value->nama_kota; ?><br>
									<?php echo $value->nama_kel; ?><br>
									<?php echo $value->nama_kec; ?>
								</td>
								<td class="text-center">
									<a href="<?php echo base_url().'hapusKK/'.$value->id_reqkk;?>" class="label label-warning" onclick="return confirm('are you sure?')">Hapus</a>
									<a href="#" class="label label-primary btn-detail" id="<?php echo $value->id_reqkk;?>">Detail</a>
								</td>
							</tr>
						<?php
					}
				}
			?>
		</tbody>
	</table>
</div>
<?php $this->load->view('general/v_footer');?>
<script type="text/javascript">
	$('.btn-detail').on('click',function(){
		var id = $(this).attr('id');
		$.ajax({
	        url: '<?php echo base_url('getDetailKK')?>',
	        type: 'post',
	        dataType: 'html',
	        data: {'data_send' : id},
	        beforeSend:function(){
	        	$('#modal-detail').modal('show');
	        	$('.modal-content').html('');
	        },
	        success: function(result) {
	        	$('.modal-content').html(result);
	        },
	        error: function(jqXHR,textStatus,errorThrown) {
	          console.log(jqXHR+" | "+textStatus+" | "+errorThrown);
	          alert('Ajax Request Failed!');
	        }
	    });
	});
</script>