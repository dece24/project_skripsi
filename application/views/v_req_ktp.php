<?php $this->load->view('general/v_header');?>
<body id="vthree">
<?php $this->load->view('general/v_frontmenu');?>
<div id="page-wrapper">
	<h2 class="b-sub-page-title"><?php echo strtoupper($page_title);?></h2>
	<hr class="b-hr"></hr>
	<?php echo form_open_multipart('prosesktp',array('class'=>'form-horizontal'))?>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Pemerintah Propinsi</label>
		    <div class="col-sm-2">
		    	<select class="form-control" name="propselect" required="required">
		    		<option value="">-pilih-</option>
		    		<?php
		    			foreach ($data_prop as $key => $value) {
		    				echo '<option value="'.$value->nama_prop.'">'.$value->nama_prop.'</option>';
		    			}
		    		?>
		    	</select>
		    </div>
	  	</div>
  		<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Pemerintah Kabupaten / Kota</label>
		    <div class="col-sm-2">
		    	<select class="form-control" name="kotaselect" required="required">
		    		<option value="">-pilih-</option>
		    		<?php
		    			foreach ($data_kota as $key => $value) {
		    				echo '<option value="'.$value->nama_kota.'">'.$value->nama_kota.'</option>';
		    			}
		    		?>
		    	</select>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Kecamatan</label>
		    <div class="col-sm-2">
		    	<select class="form-control" name="kecselect" required="required">
		    		<option value="">-pilih-</option>
		    		<?php
		    			foreach ($data_kec as $key => $value) {
		    				echo '<option value="'.$value->nama_kec.'">'.$value->nama_kec.'</option>';
		    			}
		    		?>
		    	</select>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Desa / Keluarahan</label>
		    <div class="col-sm-2">
		    	<select class="form-control" name="kelselect" required="required">
		    		<option value="">-pilih-</option>
		    		<?php
		    			foreach ($data_kel as $key => $value) {
		    				echo '<option value="'.$value->nama_kel.'">'.$value->nama_kel.'</option>';
		    			}
		    		?>
		    	</select>
		    </div>
	  	</div>
	  	<h4>Permohonan KTP</h4>
	  	<hr class="b-hr"></hr>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Tipe Permohonan</label>
		    <div class="col-sm-8">
		      	<div class="col-lg-4 no-padding-left">
				    <label class="input-group">
				      	<span class="input-group-addon">
				       		<input type="radio" aria-label="..." name="tipektp" value="baru" required="required">
				      	</span>
				      	<span class="form-control label-success">A. Baru</span>
				    </label>
				</div>
				<div class="col-lg-4 no-padding-left">
				    <label class="input-group">
				      	<span class="input-group-addon">
				       		<input type="radio" aria-label="..." name="tipektp" value="penggantian" required="required">
				      	</span>
				      	<span class="form-control label-success">B. Penggantian</span>
				    </label>
				</div>
		    </div>
	  	</div>
	  	
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Nama Lengkap</label>
		    <div class="col-sm-5">
		      	<input type="text" class="form-control" id="" value="" name="namalengkap" maxlength="50" required="required">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Foto Diri</label>
		    <div class="col-sm-5">
		      	<input type="file" class="form-control" id="" value="" name="fotodiri" required="required">
		      	<span class="alert-danger perhatian"><b>Max Size 2Mb - Format JPG|JPEG|PNG</b> Tahun Lahir Ganjil (latar foto biru) & Tahun Lahir Genap (latar foto merah)</span>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">No KK</label>
		    <div class="col-sm-5">
		      	<input type="text" min="0" pattern="[0-9]{16}" class="form-control" id="" value="" name="nokk" maxlength="30" required="required" placeholder="16 digit contoh: 12345678">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Foto KK</label>
		    <div class="col-sm-5">
		      	<input type="file" class="form-control" id="" value="" name="fotokk" required="required">
		      	<span class="alert-danger perhatian"><b>Max Size 2Mb - Format JPG|JPEG|PNG</b> Hasil Scan / Foto, Tulisan Wajib Terbaca Dengan Mudah </span>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">NIK</label>
		    <div class="col-sm-5">
		      	<input type="text" min="0" pattern="[0-9]{16}" class="form-control" id="" value="" name="nik" maxlength="30" required="required" placeholder="16 digit contoh: 12345678">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Alamat</label>
		    <div class="col-sm-5">
		      	<textarea class="form-control" name="alamat" required="required"></textarea>
		      	<input type="number" min="0" class="form-control cos-size-20 pull-left" placeholder="RT" maxlength="2" name="nort" required="required">
		      	<input type="number" min="0" class="form-control cos-size-20 pull-left" placeholder="RW" maxlength="2" name="norw" required="required">
		      	<input type="number" min="0" class="form-control cos-size-20 pull-right" placeholder="Kode Pos" maxlength="7" name="kodepos" required="required">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Mengetahui</label>
		    <div class="col-sm-5">
		      	<input type="text" class="form-control" id="" readonly="TRUE" value="<?php echo $data_meng->nama_meng;?>" name="mengnama" required="required">
		      	<input type="text" class="form-control" id="" readonly="TRUE" value="<?php echo $data_meng->nik_meng;?>" name="mengnik" required="required">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos"></label>
		    <div class="col-sm-5">
		      	<input type="submit" class="btn btn-info" value="SUBMIT">
		    </div>
	  	</div>
	</form>
</div>
<?php $this->load->view('general/v_footer');?>