<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php if(!isset($title)){ $title = 'title belum di setting'; }?>
	<title><?php echo $title;?></title>
	<link rel="stylesheet" href="<?php echo base_url().'assets/css/reset.css'?>">
	<link rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.min.css'?>">
	
    <!-- Bootstrap Core CSS -->
    <!-- <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url().'assets/vendor/metisMenu/metisMenu.min.css'?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/sb-admin-2.css'?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url().'assets/vendor/morrisjs/morris.css'?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url().'assets/vendor/font-awesome/css/font-awesome.min.css'?>" rel="stylesheet" type="text/css">

    <!-- datatables -->
    <link href="<?php echo base_url().'assets/plugin/datatables/datatables.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/plugin/jquery-ui/jquery-ui.min.css'?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/mystyles.css'?>">
</head>

<?php
	$flashmessage = $this->session->flashdata('msg');
	if(!empty($flashmessage)){
		echo '<div class="alert alert-success notif-msg" role="alert">'.$flashmessage.'
				<button type="button" class="close close-notif" aria-label="Close">
  					<span aria-hidden="true">&times;</span>
				</button>
			</div>';
	}
?>