<?php $this->load->view('general/v_header');?>
<body id="vthree">
<?php $this->load->view('general/v_frontmenu');?>
<div id="page-wrapper">
	<h2 class="b-sub-page-title"><?php echo strtoupper($page_title);?></h2>
	<hr class="b-hr"></hr>
	<?php echo  form_open_multipart(base_url('proseskk'),array('class'=>'form-horizontal')); ?>
		<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Pemerintah Propinsi</label>
		    <div class="col-sm-2">
		      	<select class="form-control" name="propselect" required="required">
		      		<option value=''>--pilih--</option>
		      		<?php foreach ($data_prop as $key => $value) {
		      			echo '<option value='.$value->nama_prop.'>'.$value->nama_prop.'</option>';
		      		} ?>
		      	</select>
		    </div>
		    
	  	</div>
  		<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Pemerintah Kabupaten / Kota</label>
		    <div class="col-sm-2">
		      	<select class="form-control" name="kotaselect" required="required">
		      		<option value=''>--pilih--</option>
		      		<?php foreach ($data_kota as $key => $value) {
		      			echo '<option value='.$value->nama_kota.'>'.$value->nama_kota.'</option>';
		      		} ?>
		      	</select>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Kecamatan</label>
		    <div class="col-sm-2">
		      	<select class="form-control" name="kecselect" required="required">
		      		<option value=''>--pilih--</option>
		      		<?php foreach ($data_kec as $key => $value) {
		      			echo '<option value='.$value->nama_kec.'>'.$value->nama_kec.'</option>';
		      		} ?>
		      	</select>
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Desa / Keluarahan</label>
		    <div class="col-sm-2">
		      	<select class="form-control" name="kelselect" required="required">
		      		<option value=''>--pilih--</option>
		      		<?php foreach ($data_kel as $key => $value) {
		      			echo '<option value='.$value->nama_kel.'>'.$value->nama_kel.'</option>';
		      		} ?>
		      	</select>
		    </div>
	  	</div>
		<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Nama Kepala Keluarga</label>
		    <div class="col-sm-5">
		      	<input type="text" class="form-control" id="" value="" name="namakepkel" required="required">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Alamat</label>
		    <div class="col-sm-5">
		      	<textarea class="form-control" name="alamat" required="required"></textarea>
		      	<input type="number" min=0 class="form-control cos-size-20 pull-left" placeholder="RT" maxlength="2" required="required" name="nort">
		      	<input type="number" min=0 class="form-control cos-size-20 pull-left" placeholder="RW" maxlength="2" required="required" name="norw">
		      	<input type="number" min=0 class="form-control cos-size-20 pull-right" placeholder="Kode Pos" maxlength="7" required="required" name="kodepos">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="" class="col-sm-3 control-label text-left-cos">Foto KK</label>
		    <div class="col-sm-5">
		      	<input type="file" name="fotokk" class="form-control" required="required">
		      	<span class="alert-danger perhatian"><b>Max Size 2Mb - Format JPG|JPEG|PNG</b> Hasil Scan / Foto, Tulisan Wajib Terbaca Dengan Mudah </span>
		    </div>
	  	</div>
	  	<h4>Data Keluarga</h4>
	  	<hr class="b-hr"></hr>
	  	<input type="button" value="add-more" class="btn btn-info add-dataktp">
	  	<div class="hidden-form-kk hidden">
		  	<div class="form-group form-kk-each">
		  		<span class="fa fa-close del-form-kk"></span>
			    <div class="col-sm-12">
			    	<div class="col-md-4">
			    		<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Nama</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" name="namainput[]">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Agama</label>
						    <div class="col-sm-9">
						      	<select class="form-control" name="agama[]">
						    		<option>-Pilih-</option>
						    		<option>Islam</option>
						    		<option>Protestan</option>
						    		<option>Katolik</option>
						    		<option>Hindu</option>
						    		<option>Budha</option>
						    	</select>
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Pekerjaan</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" name="pekerjaan[]">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Ayah</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" placeholder="Nama Ayah" name="namaayah[]">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Ibu</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" placeholder="Nama Ibu" name="namaibu[]">
						    </div>
					  	</div>
			    	</div>

			    	<div class="col-md-4">
			    		<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">NIK</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" name="nik[]">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Pendidikan</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" name="pendidikan[]">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Status</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" name="status[]">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">KWN</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" name="kwn[]" placeholder="Kewarganegaraan">
						    </div>
					  	</div>
					  	
			    	</div>

			      	<div class="col-md-4">
			    		<div class="form-group">
						    <div class="col-sm-12">
						    	<select class="form-control" name="kelamin[]">
						    		<option>-Jenis Kelamin-</option>
						    		<option>Pria</option>
						    		<option>Wanita</option>
						    	</select>
						    </div>
					  	</div>
					  	<div class="form-group">
						    <div class="col-sm-12">
						      	<input type="text" class="form-control datepicker" id="" value="" name="tanggal[]" placeholder="Tanggal Lahir">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <div class="col-sm-12">
						      	<input type="text" class="form-control" id="" value="" name="hubungan[]" placeholder="Hubungan dalam keluarga">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <div class="col-sm-12">
						      	<input type="number" class="form-control" id="" value="" name="passport[]" placeholder="No Passport">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <div class="col-sm-12">
						      	<input type="number" class="form-control" id="" value="" name="kitas[]" placeholder="No Kitas">
						    </div>
					  	</div>
			    	</div>
			    </div>
		  	</div>
	  	</div>
	  	<div class="form-group form-kk-each">
		  		<span class="fa fa-close del-form-kk"></span>
			    <div class="col-sm-12">
			    	<div class="col-md-4">
			    		<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Nama</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" name="namainput[]">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Agama</label>
						    <div class="col-sm-9">
						      	<select class="form-control" name="agama[]">
						    		<option>-Pilih-</option>
						    		<option>Islam</option>
						    		<option>Protestan</option>
						    		<option>Katolik</option>
						    		<option>Hindu</option>
						    		<option>Budha</option>
						    	</select>
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Pekerjaan</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" name="pekerjaan[]">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Ayah</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" placeholder="Nama Ayah" name="namaayah[]">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Ibu</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" placeholder="Nama Ibu" name="namaibu[]">
						    </div>
					  	</div>
			    	</div>

			    	<div class="col-md-4">
			    		<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">NIK</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" name="nik[]">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Pendidikan</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" name="pendidikan[]">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">Status</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" name="status[]">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <label for="" class="col-sm-3 control-label text-left-cos">KWN</label>
						    <div class="col-sm-9">
						      	<input type="text" class="form-control" id="" value="" name="kwn[]" placeholder="Kewarganegaraan">
						    </div>
					  	</div>
					  	
			    	</div>

			      	<div class="col-md-4">
			    		<div class="form-group">
						    <div class="col-sm-12">
						    	<select class="form-control" name="kelamin[]">
						    		<option>-Jenis Kelamin-</option>
						    		<option>Pria</option>
						    		<option>Wanita</option>
						    	</select>
						    </div>
					  	</div>
					  	<div class="form-group">
						    <div class="col-sm-12">
						      	<input type="text" class="form-control datepicker" id="" value="" name="tanggal[]" placeholder="Tanggal Lahir">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <div class="col-sm-12">
						      	<input type="text" class="form-control" id="" value="" name="hubungan[]" placeholder="Hubungan dalam keluarga">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <div class="col-sm-12">
						      	<input type="number" class="form-control" id="" value="" name="passport[]" placeholder="No Passport">
						    </div>
					  	</div>
					  	<div class="form-group">
						    <div class="col-sm-12">
						      	<input type="number" class="form-control" id="" value="" name="kitas[]" placeholder="No Kitas">
						    </div>
					  	</div>
			    	</div>
			    </div>
		  	</div>
	  	<div class="addmore-container">
	  		
	  	</div>
	  	<input type="submit" class="btn btn-primary" value="SUBMIT">
	</form>
</div>
<?php $this->load->view('general/v_footer');?>