<?php $this->load->view('general/v_header');?>
<body id="vone">
<div class="container vi-header" style="background-image: url('<?php echo base_url().'assets/image/'.$wall_1;?>')">
  <a href="<?php echo base_url();?>">
    <img src="<?php echo base_url().'assets/image/'.$icon;?>" class="vi-h-img">
    <h5 class="vi-h-h5"><?php echo $nama_web; ?></h5>
  </a>
</div>

  <div class="container">
    <div class="row">

      	<div class="col-sm-9 col-md-7 col-lg-5 mx-auto mx-auto-cos">
        	<div class="card card-signin my-5">
          		<div class="card-body">
            		<h5 class="card-title text-center">Registrasi</h5>
            	<!-- <form class="form-signin"> -->
            	<?php echo form_open('uprocess',array('class'=>'form-signin'));?>
                <div class="form-group">
                  <label for="email">Nama Lengkap</label>
                  <input type="text" id="namalengkap" name="namalengkap" class="form-control" placeholder="Nama Lengkap" autofocus value="<?php echo set_value('namalengkap'); ?>">
                  <!-- <label for="username">Username</label> -->
                  <?php echo form_error('namalengkap', '<div class="error">', '</div>'); ?>
                </div>
              	<div class="form-group">
              		<label for="email">Email</label>
                	<input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" value="<?php echo set_value('email'); ?>">
                	<?php echo form_error('email', '<div class="error">', '</div>'); ?>
              	</div>
              	<div class="form-group">
              		<label for="email">Username</label>
                	<input type="text" id="username" name="username" class="form-control" placeholder="username" value="<?php echo set_value('username'); ?>">
                	<!-- <label for="username">Username</label> -->
                	<?php echo form_error('username', '<div class="error">', '</div>'); ?>
              	</div>
              	<div class="form-group">
              		<label for="email">Password</label>
                	<input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password (min 6 character)" >
                	<?php echo form_error('password', '<div class="error">', '</div>'); ?>
              	</div>
              	<div class="form-group">
              		<label for="email">Repeat Password</label>
                	<input type="password" id="re-inputPassword" name="repassword" class="form-control" placeholder="Password confirmation" >
                	<?php echo form_error('repassword', '<div class="error">', '</div>'); ?>
              	</div>

             	<div>
	              	<button class="btn btn-lg btn-primary text-uppercase" type="submit">Submit</button>
	              	<button class="btn btn-lg btn-primary text-uppercase" type="reset">Reset</button>
          		</div>
              	<a href="<?php echo base_url();?>">Login</a>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php $this->load->view('general/v_footer');?>