<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengecekan
{
    function __construct() {
        $this->ci =& get_instance();

        // $this->AdminCheck();
        // $this->UserCheck();
    }

    public function AdminCheck()
    {
    	$this->ci->load->library('session');
    	$this->ci->load->model('m_general');

    	$id_admin = $this->ci->session->userdata('idAdm');
    	$options = array(
    				'id_admin' => $id_admin,
    				'admin_active' => 1
    			);
    	$cek_admin = $this->ci->m_general->get_single_spek('tb_admin','id_admin',$options);
    	if(empty($cek_admin)){
    		redirect('back');
    	}else{
            return true;
        }
    }

    public function UserCheck()
    {
        $this->ci->load->library('session');
        $this->ci->load->model('m_general');

        $id_user = $this->ci->session->userdata('idUser');
        $options = array(
                    'id_user' => $id_user,
                    'is_active' => 1
                );
        $cek_user = $this->ci->m_general->get_single_spek('tb_user','id_user',$options);
        if(empty($cek_user)){
            redirect(base_url());
        }else{
            return true;
        }
    }
}